// import { Auth,User,Activity,TypeData} from '../../components/index'
import { Api } from "osp-client-js";
import dynamic from "next/dynamic";

const ConnectWallet = dynamic(
  // @ts-ignore
  () => import("../../components/index").then((T) => T.ConnectWallet),
  {
    ssr: false,
  }
);
const Auth = dynamic(
  () => import("../../components/index").then((T) => T.Auth),
  {
    ssr: false,
  }
);
const Profile = dynamic(
  () => import("../../components/index").then((T) => T.Profile),
  {
    ssr: false,
  }
);
const Activity = dynamic(
  () => import("../../components/index").then((T) => T.Activity),
  {
    ssr: false,
  }
);
const TypeData = dynamic(
  () => import("../../components/index").then((T) => T.TypeData),
  {
    ssr: false,
  }
);
const Relation = dynamic(
  () => import("../../components/index").then((T) => T.Relation),
  {
    ssr: false,
  }
);
const Reaction = dynamic(
  () => import("../../components/index").then((T) => T.Reaction),
  {
    ssr: false,
  }
);

const Faucet = dynamic(
  () => import("../../components/index").then((T) => T.Faucet),
  {
    ssr: false,
  }
);

const Community = dynamic(
  () => import("../../components/index").then((T) => T.Community),
  {
    ssr: false,
  }
);

export function App() {
  const apiClient = new Api({
    baseUrl:
      typeof location !== "undefined" ? location?.origin + "/dev_api/v2" : "",
    securityWorker: (secureData) => secureData,
  });
  return (
    <div>
      <h1>RestAPI接口请求Example：</h1>
      {/* @ts-ignore */}
      <ConnectWallet apiClient={apiClient}>
        <Auth />
        <Profile />
        <Community />
        <Faucet />
        <Relation />
        <Activity />

        <h2 style={{ color: "red" }}>以上已联调成功</h2>

        <TypeData />
        <Reaction />
      </ConnectWallet>
    </div>
  );
}
