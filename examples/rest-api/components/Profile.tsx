// import { useEffect, useState } from "react";

export const Profile = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: any;
}) => {
  const changeProfile = async () => {
    const res = await apiClient.profiles.updateProfile("0x7", {
      background: "hihihi",
    });
    console.log("ppppp", res);
  };

  const getAllProfiles = async () => {
    const res = await apiClient.profiles.listProfile({
      address: "0xa0ee7a142d267c1f36714e4a8f75612f20a79720",
    });
    console.log("ppppp", res);
  };

  const getProfile = async (profileId) => {
    const res = await apiClient.profiles.getProfileById("0x7");
    console.log("ppppp", res);
  };

  const getAsset = async (profileId) => {
    const res = await apiClient.profiles.getAssetsByProfileId("0x8", {
      type: "NFT",
      limit: 20,
      next_token: "",
    });
    console.log("ppppp", res);
  };
  return (
    <div>
      <h2>Profile模块</h2>

      <h3>修改用户的profile</h3>
      <button onClick={changeProfile}>修改用户的profile</button>

      <br />
      <h3>获取用户所有的profile</h3>
      <button onClick={getAllProfiles}>获取用户的所有profile</button>
      <br />
      <h3>查询用户某一个profile</h3>
      <button onClick={getProfile}>获取用户的profile</button>
      <h3>查询用户某一个profile的asset</h3>
      <button onClick={getAsset}>获取用户的asset</button>
    </div>
  );
};
