// import { useEffect, useState } from "react";

export const Faucet = ({
  address,
  apiClient,
}: {
  address?: string;
  apiClient?: any;
}) => {
  const mintSlotNft = async () => {
    const res = await apiClient.mint.mintSlotNft({ address });
  };

  return (
    <div>
      <h2>Faucet模块</h2>

      <h3>mintSlotNft</h3>
      <button onClick={mintSlotNft}>mintSlotNft</button>
    </div>
  );
};
