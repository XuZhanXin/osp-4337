import React from "react";
import { useState } from "react";

const feedSlug = "user";
// 关注人 不为 NullFollowModule时，需要转profileId —— 重复关注报错，需提供新userId
const userId = "0x127";

export function Relation({ client }) {
  // 关注人为 ProfileFollowModule
  const body = {
    followModuleParam: {
      type: "ProfileFollowModule",
      data: {
        profileId: client.profile.profileId,
      },
    },
  };
  const [followerList, setFollowerList] = useState([]);
  const [followingList, setFollowingList] = useState([]);

  const handleGetFollowingList = async () => {
    const profileId = client.profile.profileId;
    const queryBody = {
      with_reverse: true,
      limit: 20,
      next_token: "",
      include_target_ids: [],
      exclude_target_ids: [],
    };
    const { error, data }: any = await client.relation.following(
      profileId,
      queryBody
    );

    setFollowingList(data.rows || []);

    console.log("handleGetFollowingList info", data, error);
  };

  const handleGetFollowerList = async () => {
    const profileId = client.profile.profileId;
    const queryBody = {
      with_reverse: true,
      limit: 20,
      next_token: "",
      include_target_ids: [],
      exclude_target_ids: [],
    };
    const { error, data }: any = await client.relation.followers(
      profileId,
      queryBody
    );
    setFollowerList(data.rows || []);
    console.log("handleGetFollowerList info", data, error);
  };

  const followWithChainOnly = async (profileId) => {
    const queryBody = {
      follow_module_param: {
        type: "NULL_FOLLOW_MODULE",
        init: {},
      },
    };
    const { error, data }: any = await client.relation.follow(
      profileId,
      queryBody,
      true,
      OnChainTypeEnum.ONLY_ON_CHAIN
    );
    if (data?.code === 200) {
      alert("关注成功");
    }
    console.log("handleDoFollow info", data, error);
  };

  const followWithChain = async (profileId) => {
    const queryBody = {
      follow_module_param: {
        type: "NULL_FOLLOW_MODULE",
        init: {},
      },
    };
    const { error, data }: any = await client.relation.follow(
      profileId,
      queryBody,
      true,
      OnChainTypeEnum.ON_CHAIN
    );
    if (data?.code === 200) {
      alert("关注成功");
    }
    console.log("handleDoFollow info", data, error);
  };

  const follow = async (profileId) => {
    const queryBody = {
      follow_module_param: {
        type: "NULL_FOLLOW_MODULE",
        init: {},
      },
    };
    const { error, data }: any = await client.relation.follow(
      profileId,
      queryBody,
      false
    );

    console.log("handleDoFollow info", data, error);
  };

  const unFollowWithChain = async (profileId) => {
    const { error, data }: any = await client.relation.unFollow(
      profileId,
      true
    );

    console.log("handleDoFollow info", data, error);
  };

  const unFollow = async (profileId) => {
    const { error, data }: any = await client.relation.unFollow(
      profileId,
      false
    );

    console.log("handleDoFollow info", data, error);
  };

  return (
    <>
      <h2>Relation 模块</h2>
      <p>
        <h3>获取关注列表</h3>
        <button onClick={handleGetFollowingList}>获取关注列表</button>
        <p>
          {followingList.map((item, index) => {
            return (
              <>
                <div key={`${item.owner}:${index}`}>
                  {item.owner}
                  {item.profile_id} --- {item.handle}
                </div>
                <button onClick={() => unFollowWithChain(item.profile_id)}>
                  取消关注--链上
                </button>
                <button onClick={() => unFollow(item.profile_id)}>
                  取消关注--链下
                </button>
              </>
            );
          })}
        </p>
      </p>
      <p>
        <h3>获取粉丝列表</h3>
        <button onClick={handleGetFollowerList}>获取粉丝列表</button>
        <p>
          {followerList.map((item, index) => {
            return (
              <>
                <div key={`${item.owner}:${index}`}>
                  {item.owner}
                  {item.profile_id} --- {item.handle}
                </div>
                <button onClick={() => followWithChain(item.profile_id)}>
                  关注用户--链上
                </button>
                <button onClick={() => follow(item.profile_id)}>
                  关注用户--链下
                </button>
                <button onClick={() => followWithChainOnly(item.profile_id)}>
                  关注用户--只上链
                </button>
              </>
            );
          })}
        </p>
      </p>
      {/*      <p>
        <h3>关注单个用户</h3>
        <button onClick={handleDoFollow}>doFollow</button>
      </p>
*/}

      {/*      <p>
        <h3>关注单个用户:handleDoFollowBroadCast</h3>
        <button onClick={handleDoFollowBroadCast}>doFollow</button>
      </p>
      <h3>关注单个用户---通过PostWithSig</h3>
      <button onClick={createRelationPostWithSig}>关注单个用户-abi</button>
      <br />
      <h3>关注单个用户---通过Post</h3>
      <button onClick={createRelationPost}>关注单个用户-abi</button>
      <br />*/}

      {/*      <p>
        <h3>批量关注、取关</h3>
        <p>后端暂未开发完成</p>
      </p>*/}
    </>
  );
}
