// @ts-nocheck

import { OspRes } from "./constant";
import { Api } from "./rest_api_generated";

export class Request {
  api: any;
  baseUrl: string;
  errorHandler?: () => void;

  constructor(baseUrl: string, errorHandler) {
    this.baseUrl = baseUrl;
    this.errorHandler = errorHandler || (() => {});
    this.init();
  }

  init() {
    this.api = new Api({
      baseUrl: this.baseUrl,
      securityWorker: (secureData) => secureData,
    });
  }

  async call(path, ...rest): Promise<OspRes> {
    try {
      const data = await this.api[path[0]][path[1]](...rest);
      if (data.error) {
        this.errorHandler({
          ...data.error,
          code: data.error?.code,
        });
      }
      return { data: data.data.data, error: data.error };
    } catch (e) {
      //To-do: report error to cloud service
      if (e.status !== 200) {
        const res = {
          error: { ...e.error, code: e.error?.code },
          data: null,
        };
        this.errorHandler({
          error: { ...e.error, code: e.error?.code },
          status: e.status,
        });
        return res;
      }
    }
  }
}
