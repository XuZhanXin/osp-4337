# brew install jq
version=`jq '.version' package.json | tr -d '"'`


cd ~/work/kikitrade_web
git checkout dapps-beta  
git pull origin dapps-beta
git checkout dapps-beta-osp-client-js
git merge dapps-beta
cd packages/ui
echo osp-client-js@"$version"
pnpm add osp-client-js@"$version"
git commit -am "update osp-client-js version ${version}"
git push origin dapps-beta-osp-client-js
git checkout dapps-beta  
git merge dapps-beta-osp-client-js
git push origin  dapps-beta  