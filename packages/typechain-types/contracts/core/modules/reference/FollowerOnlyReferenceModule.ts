/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */
import type {
  BaseContract,
  BigNumber,
  BigNumberish,
  BytesLike,
  CallOverrides,
  PopulatedTransaction,
  Signer,
  utils,
} from "ethers";
import type { FunctionFragment, Result } from "@ethersproject/abi";
import type { Listener, Provider } from "@ethersproject/providers";
import type {
  TypedEventFilter,
  TypedEvent,
  TypedListener,
  OnEvent,
  PromiseOrValue,
} from "../../../../common";

export interface FollowerOnlyReferenceModuleInterface extends utils.Interface {
  functions: {
    "OSP()": FunctionFragment;
    "initializeReferenceModule(uint256,uint256,bytes)": FunctionFragment;
    "processReaction(uint256,uint256,uint256,bytes)": FunctionFragment;
  };

  getFunction(
    nameOrSignatureOrTopic:
      | "OSP"
      | "initializeReferenceModule"
      | "processReaction"
  ): FunctionFragment;

  encodeFunctionData(functionFragment: "OSP", values?: undefined): string;
  encodeFunctionData(
    functionFragment: "initializeReferenceModule",
    values: [
      PromiseOrValue<BigNumberish>,
      PromiseOrValue<BigNumberish>,
      PromiseOrValue<BytesLike>
    ]
  ): string;
  encodeFunctionData(
    functionFragment: "processReaction",
    values: [
      PromiseOrValue<BigNumberish>,
      PromiseOrValue<BigNumberish>,
      PromiseOrValue<BigNumberish>,
      PromiseOrValue<BytesLike>
    ]
  ): string;

  decodeFunctionResult(functionFragment: "OSP", data: BytesLike): Result;
  decodeFunctionResult(
    functionFragment: "initializeReferenceModule",
    data: BytesLike
  ): Result;
  decodeFunctionResult(
    functionFragment: "processReaction",
    data: BytesLike
  ): Result;

  events: {};
}

export interface FollowerOnlyReferenceModule extends BaseContract {
  connect(signerOrProvider: Signer | Provider | string): this;
  attach(addressOrName: string): this;
  deployed(): Promise<this>;

  interface: FollowerOnlyReferenceModuleInterface;

  queryFilter<TEvent extends TypedEvent>(
    event: TypedEventFilter<TEvent>,
    fromBlockOrBlockhash?: string | number | undefined,
    toBlock?: string | number | undefined
  ): Promise<Array<TEvent>>;

  listeners<TEvent extends TypedEvent>(
    eventFilter?: TypedEventFilter<TEvent>
  ): Array<TypedListener<TEvent>>;
  listeners(eventName?: string): Array<Listener>;
  removeAllListeners<TEvent extends TypedEvent>(
    eventFilter: TypedEventFilter<TEvent>
  ): this;
  removeAllListeners(eventName?: string): this;
  off: OnEvent<this>;
  on: OnEvent<this>;
  once: OnEvent<this>;
  removeListener: OnEvent<this>;

  functions: {
    OSP(overrides?: CallOverrides): Promise<[string]>;

    initializeReferenceModule(
      profileId: PromiseOrValue<BigNumberish>,
      contentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<[void]>;

    processReaction(
      profileId: PromiseOrValue<BigNumberish>,
      referencedProfileId: PromiseOrValue<BigNumberish>,
      referencedContentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<[void]>;
  };

  OSP(overrides?: CallOverrides): Promise<string>;

  initializeReferenceModule(
    profileId: PromiseOrValue<BigNumberish>,
    contentId: PromiseOrValue<BigNumberish>,
    data: PromiseOrValue<BytesLike>,
    overrides?: CallOverrides
  ): Promise<void>;

  processReaction(
    profileId: PromiseOrValue<BigNumberish>,
    referencedProfileId: PromiseOrValue<BigNumberish>,
    referencedContentId: PromiseOrValue<BigNumberish>,
    data: PromiseOrValue<BytesLike>,
    overrides?: CallOverrides
  ): Promise<void>;

  callStatic: {
    OSP(overrides?: CallOverrides): Promise<string>;

    initializeReferenceModule(
      profileId: PromiseOrValue<BigNumberish>,
      contentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<void>;

    processReaction(
      profileId: PromiseOrValue<BigNumberish>,
      referencedProfileId: PromiseOrValue<BigNumberish>,
      referencedContentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<void>;
  };

  filters: {};

  estimateGas: {
    OSP(overrides?: CallOverrides): Promise<BigNumber>;

    initializeReferenceModule(
      profileId: PromiseOrValue<BigNumberish>,
      contentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<BigNumber>;

    processReaction(
      profileId: PromiseOrValue<BigNumberish>,
      referencedProfileId: PromiseOrValue<BigNumberish>,
      referencedContentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<BigNumber>;
  };

  populateTransaction: {
    OSP(overrides?: CallOverrides): Promise<PopulatedTransaction>;

    initializeReferenceModule(
      profileId: PromiseOrValue<BigNumberish>,
      contentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;

    processReaction(
      profileId: PromiseOrValue<BigNumberish>,
      referencedProfileId: PromiseOrValue<BigNumberish>,
      referencedContentId: PromiseOrValue<BigNumberish>,
      data: PromiseOrValue<BytesLike>,
      overrides?: CallOverrides
    ): Promise<PopulatedTransaction>;
  };
}
